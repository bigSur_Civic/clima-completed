#  Clima

## Function:

Check the weather for the current location based on the GPS. data from the iPhone as well as by searching for a city manually.  

## Detail:

* Swift protocols, Swift computed properties, Swift closure and completion handlers
* Use URLSession to network and make HTTP requests
* Parse JSON with the native Encodable and Decodable protocols 
* Use Core Location to get the current location from the phone GPS

